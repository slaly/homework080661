<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
 </head>
  <body>
  
<div class="container">
  <div class="row">
    <div class="col-md-12">
    <h3 align="center"> Register </h3>
      <form action="" method="POST" name="register"  id="register">
        <table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
            <td align="right"> E-mail &nbsp; </td>
            <td><input name="Email" type="email" id="Email" class="form-control" placeholder="เช่น abc@gmail.com " required></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
		  <tr>
            <td align="right"> Comfirm&nbsp; E-mail&nbsp;</td>
            <td>
            <input name="Email2" type="email" id="email2" placeholder="กรอก email อีกครั้ง " class="form-control"  required >        
</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
		  <tr>
            <td align="right"> First name &nbsp;</td>
            <td colspan="2">
            <input name="Fname" type="text" id="Fname"  class="form-control" placeholder="ภาษาไทยหรืออังกฤษ" required></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right">Last name &nbsp;</td>
            <td colspan="2">
            <input name="Lname" type="text" id="Lname" size="50" class="form-control" placeholder="ภาษาไทยหรืออังกฤษ" required></td>
            <td>&nbsp;</td>
          </tr>
	<tr>
            <td align="right"> Password &nbsp;</td>
            <td><input name="Password" type="password" id="Password" class="form-control" placeholder="อย่างน้อย 6 ตัว"  required></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right"> Comfirm&nbsp; Password&nbsp;</td>
            <td>
            <input name="Password2" type="password" id="Password2" placeholder="กรอกรหัสผ่าน อีกครั้ง " class="form-control"  required >        
</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          
          
          <tr>
            <td align="center">&nbsp;</td>
            <td colspan="3" align="center">
            </td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td colspan="3" align="left">
            <input type="submit" name="regis" id="regis" class="btn btn-info btn-sm" value="SUBMIT" >
            
            </td>
          </tr>
        </table>
      </form>
      </div>
    </div>
  </div>
 


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script> 





<script>
$.validator.addMethod( "Firstchar", function( value, element ) { 
    if (this.optional(element)) {
  return true;
 }
    return /[A-Z]/.test(value);
}, "not start A - Z" );

$.validator.addMethod( "password", function( value, element ) { 
    if (this.optional(element)) {
  return true;
 }
    return /[A-Za-z0-9]$/.test(value);
}, "Must a-z A-Z 0-9" );

$("#register").validate({
    rules :{
        Fname:{
           Firstchar : true
        },
        Lname:{
           Firstchar : true
        },
        Email:{
           required : true
        },
        Email2:{
            required:true,
            email:true,
            equalTo: "#email2"

        },
        Password :{
            required: true,
            password : true
        },
        Password2 :{
            required: true,
            password : true,
            equalTo: "#Password2"
        }

    },
    messages : {
  firstname:{
            required: 'กรุณาใส่ Firstname', 
            Firstchar : 'First Char Capital character'
        },  
        lastname:{
            required: 'กรุณาใส่ค่า Lastname', 
            Firstchar : 'First Char Capital character'
        },
        Email :{
            required: 'กรุณาใส่ email' 
        },
  Email2: {
   required:'กรุณาใส่ email',
            Email:'กรุณาใส่ email ให้ถูกต้อง',
            equalTo: "email value same as email"
  },
  Password :{
            required: 'กรุณาใส่ password' 
        },
        Password2 :{
            required:  'กรุณาใส่ password' ,
            equalTo: "password value same as password"
        }
    } 

});

</script>



  </body>
</html>